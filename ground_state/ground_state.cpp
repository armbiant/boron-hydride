#include <inq/inq.hpp>

#include <openbabel/atom.h>         // for OpenBabel::OBAtom
#include <openbabel/generic.h>      // for OpenBabel::OBUnitCell
#include <openbabel/mol.h>          // for OpenBabel::OBMol
#include <openbabel/obconversion.h> // for OpenBabel::OBConversion

#include <units/quantity_io.h>
#include <units/isq/si/mass.h>
#include <units/isq/si/length.h>

#include <filesystem>
#include <functional> // for std::invoke
#include <iterator> // for std::next

#undef NDEBUG
#include <cassert>

namespace fish {

using molecule = OpenBabel::OBMol;

struct cannot : std::runtime_error {
  cannot(std::string const &action_descr, std::string const &reason_descr)
      : std::runtime_error{"cannot " + action_descr + " because " +
                           reason_descr} {}
};

[[noreturn]] void cant(std::string action_description) {
  try {
    throw;
  } catch (std::exception const &e) {
    throw cannot{action_description, e.what()};
  }
  throw cannot{action_description, "unknown reason"};
}

class atom_const_iterator {
  OpenBabel::OBAtomIterator impl_;
  atom_const_iterator(OpenBabel::OBAtomIterator impl) : impl_{impl} {}
  friend class atoms_type;

public:
  using difference_type = OpenBabel::OBAtomIterator::difference_type;
  using value_type = OpenBabel::OBAtom;
  using pointer = OpenBabel::OBAtom const *;
  using reference = OpenBabel::OBAtom const &;
  using iterator_category = OpenBabel::OBAtomIterator::iterator_category;

  auto operator++() -> atom_const_iterator & {
    ++impl_;
    return *this;
  }
  auto operator--() -> atom_const_iterator & {
    --impl_;
    return *this;
  }
  auto operator+=(difference_type n) -> atom_const_iterator & {
    impl_ += n;
    return *this;
  }
  auto operator-=(difference_type n) -> atom_const_iterator & {
    impl_ += n;
    return *this;
  }

  auto operator*() const -> reference { return **impl_; }
  auto operator->() const -> pointer { return *impl_; }

  bool operator==(atom_const_iterator const &o) const {
    return impl_ == o.impl_;
  }
  bool operator!=(atom_const_iterator const &o) const {
    return impl_ != o.impl_;
  }
};

class atoms_type {
  OpenBabel::OBAtomIterator begin_;
  OpenBabel::OBAtomIterator end_;
  unsigned int size_;

  atoms_type &operator=(atoms_type &&) = delete;
  atoms_type(OpenBabel::OBAtomIterator begin, OpenBabel::OBAtomIterator end, unsigned int size)
      : begin_{begin}, end_{end}, size_{size} {}
  friend auto atoms(OpenBabel::OBMol const &);

public:
  auto begin() const { return atom_const_iterator{begin_}; };
  auto end() const { return atom_const_iterator{end_}; };
  auto size() const { return size_; }
};

auto atoms(OpenBabel::OBMol const &mol) {
  return atoms_type{const_cast<OpenBabel::OBMol &>(mol).BeginAtoms(),  // or OpenBabel::OBMolAtomIter(mol)
                    const_cast<OpenBabel::OBMol &>(mol).EndAtoms(),    // some centinel
                    mol.NumAtoms()};
}

namespace conversion {

void read(std::istream &is, OpenBabel::OBMol &mol, std::string_view fmt) try {
  OpenBabel::OBConversion cnv{&is};
  if (not cnv.SetInFormat(fmt.data())) {
    throw std::runtime_error{"cannot find in format `" + std::string{fmt} +
                             "` check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.Read(&mol)) {
    throw std::runtime_error{"cannot read format `" + std::string{fmt} +
                             "`, check consistency and that BABEL_DATADIR "
                             "points to data (txt files)"};
  }
} catch (...) {
  cant("fish::read from stream into molecule as format `" + std::string{fmt});
}

void read_file(OpenBabel::OBMol &mol, std::string const &filename,
               std::string_view fmt) try {
  OpenBabel::OBConversion cnv;
  if (not cnv.SetInFormat(fmt.data())) {
    throw std::runtime_error{"cannot find format `" + std::string{fmt} +
                             "`, check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.ReadFile(&mol, filename.c_str())) {
    throw std::runtime_error{
        "OBConversion::ReadFile failed, check consistency and that "
        "BABEL_DATADIR points to data (txt files)"};
  }
  std::clog << "Read " << mol.NumAtoms() << " from file " << filename
            << std::endl;
} catch (std::exception &e) {
  throw cannot{"fish::read file `" + filename + "` as format `" +
                   std::string{fmt},
               e.what()};
}

//! "fish::read molecule from path `"+ p.string() +"` as format `"+
//! std::string{fmt} +"`"
void read(OpenBabel::OBMol &mol, std::filesystem::path const &p,
          std::string_view fmt) try {
  if (not exists(p)) {
    throw std::runtime_error("path `" + p.string() + "` does not exist.");
  }
  return read_file(mol, p.string(), fmt);
} catch (std::exception &e) {
  throw cannot{"fish::read molecule from path `" + p.string() +
                   "` as format `" + std::string{fmt} + "`",
               e.what()};
}

void read(OpenBabel::OBMol &mol, std::filesystem::path const& p) try {
  if(p.filename().string() == "POSCAR") {return read(mol, p, "POSCAR");}
  if(p.extension().string().size() < 2) throw std::runtime_error("cannot derive format from extension `"+ p.extension().string() +" or stem "+ p.stem().string());
  return read(mol, p, p.extension().string().substr(1).c_str());
} catch (std::exception &e) {
  throw cannot{"fish::read molecule from path `" + p.string(), e.what()};
}

void write_file(OpenBabel::OBMol const &mol, std::string const &filename,
                std::string_view fmt) try {
  OpenBabel::OBConversion cnv;
  if (not cnv.SetOutFormat(fmt.data())) {
    throw std::runtime_error{"cannot find out format `" + std::string{fmt} +
                             "` check that BABEL_LIBDIR points to plugins"};
  }
  if (not cnv.WriteFile(&const_cast<OpenBabel::OBMol &>(mol),
                        filename.c_str())) {
    throw std::runtime_error{
        "cannot write file `" + filename + "` with format format `" +
        std::string{fmt} +
        "`, check consistency and that env var BABEL_DATADIR "
        "points to data (txt files). Check if the molecule is compatible with "
        "the format (e.g. lacks fields needed by the format)"};
  }
} catch (...) {
  cant("fish::write_file molecule in filename `" + filename + "`");
}

void write(OpenBabel::OBMol const &mol, std::filesystem::path const &p,
           std::string_view fmt) {
  return write_file(mol, p.string(), fmt);
}
void write(OpenBabel::OBMol const &mol, std::filesystem::path const &p) {
  return write(mol, p, p.extension().string().substr(1).c_str());
}

} // end namespace conversion

void fill(OpenBabel::OBUnitCell const &uc, OpenBabel::OBMol &mol) {
  const_cast<OpenBabel::OBUnitCell &>(uc).FillUnitCell(&mol);
}

auto const& unit_cell(OpenBabel::OBMol const& mol) {
  assert(const_cast<OpenBabel::OBMol&>(mol).HasData(OpenBabel::OBGenericDataType::UnitCell));  // loaded format might not have cell
  return static_cast<OpenBabel::OBUnitCell&>(*const_cast<OpenBabel::OBMol&>(mol).GetData(OpenBabel::OBGenericDataType::UnitCell));
}

void fill_unit_cell(OpenBabel::OBMol &mol) {
  assert(mol.HasData(OpenBabel::OBGenericDataType::UnitCell));  // loaded format might not have cell
  fill(static_cast<OpenBabel::OBUnitCell &>(
           *mol.GetData(OpenBabel::OBGenericDataType::UnitCell)),
       mol);
}

auto volume(OpenBabel::OBUnitCell const& uc) {
//  using namespace units::isq::si::iau::references;  // (1.0*angstrom)*(1.0*angstrom)*(1.0*angstrom)
  using namespace units::isq::si::references; // m
  return uc.GetCellVolume()*(1.0e-10*m)*(1.0e-10*m)*(1.0e-10*m);
}

auto exact_mass(OpenBabel::OBMol const& mol) {
  using namespace units::isq::si::references; // Da
  return const_cast<OpenBabel::OBMol&>(mol).GetExactMass()*(1.66054e-27*kg);
}

class vector_3D : OpenBabel::vector3 {
  template<class X, class Y, class Z>
  vector_3D(OpenBabel::vector3 v3) : OpenBabel::vector3{v3} {}
  friend auto vectors(OpenBabel::OBUnitCell const&);
 public:
  auto x() const {
    using namespace units::isq::si::references; // m
    return vector3::x()*(1.0e-10*m);
  }
  auto y() const {
    using namespace units::isq::si::references; // m
    return vector3::y()*(1.0e-10*m);
  }
  auto z() const {
    using namespace units::isq::si::references; // m
    return vector3::z()*(1.0e-10*m);
  }
};

auto vectors(OpenBabel::OBUnitCell const& c) {
  auto vecs = c.GetCellVectors();
  return std::array<OpenBabel::vector3, 3>{vecs[0], vecs[1], vecs[2]};
}

} // end namespace fish

template<class T>
void what(T const&) = delete;

int main(int /*argc*/, char ** /*argv*/) {

  auto const mol = std::invoke([] {
    OpenBabel::OBMol mol;
    fish::conversion::read(mol, "POSCAR");  // fish::conversion::read(mol, "B104.67H3_allatoms.cif");
    assert(fish::atoms(mol).size() == 327);
    return mol;
  });

  std::cout<<"density "<< fish::exact_mass(mol)/fish::volume(fish::unit_cell(mol)) << std::endl;

  std::cout
    << fish::vectors(fish::unit_cell(mol))[0].x() <<' '
    << fish::vectors(fish::unit_cell(mol))[0].y() <<' '
    << fish::vectors(fish::unit_cell(mol))[0].z() <<'\n'

    << fish::vectors(fish::unit_cell(mol))[1].x() <<' '
    << fish::vectors(fish::unit_cell(mol))[1].y() <<' '
    << fish::vectors(fish::unit_cell(mol))[1].z() <<'\n'

    << fish::vectors(fish::unit_cell(mol))[2].x() <<' '
    << fish::vectors(fish::unit_cell(mol))[2].y() <<' '
    << fish::vectors(fish::unit_cell(mol))[2].z() <<'\n'

    << std::endl
  ;

  using namespace inq::magnitude;

  auto box = inq::systems::box::orthorhombic(10.0_b, 10.0_b, 12.0_b).cutoff_energy(40.0_Ha);

}
